#include "header.h"

EnemysField::EnemysField() : Field(100,0){
    for(int i = 3, j = 0; j <= 3, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 7, j = 0; j <= 7, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 2; j <= 9, i >= 2; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 6; j <= 9, i >= 6; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 1, j = 0; j <= 1, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 5, j = 0; j <= 5, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 0; j <= 9, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 4; j <= 9, i >= 4; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 8; j <= 9, i >= 8; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 0, j = 0; j <= 0, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 2, j = 0; j <= 2, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 4, j = 0; j <= 4, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 6, j = 0; j <= 6, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 8, j = 0; j <= 8, i >= 0; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 1; j <= 9, i >= 1; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 3; j <= 9, i >= 3; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 5; j <= 9, i >= 5; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 7; j <= 9, i >= 7; i--, j++)
        Free.push_back(std::make_pair(i, j));
    for(int i = 9, j = 9; j <= 9, i >= 9; i--, j++)
        Free.push_back(std::make_pair(i, j));
    free_f = 100;
}

void EnemysField::get_coordinates(int &x, int &y){
    int x_tmp, y_tmp;
    y_tmp = (*(Free.begin())).first;                   // OH MY GOD!!! ITS PERVERSIONS!!!
    x_tmp = (*(Free.begin())).second;
    while(Field[y_tmp*10 + x_tmp] != 0) {
        Free.pop_front();
        y_tmp = (*(Free.begin())).first;                   // ANOTHER ONE!!!
        x_tmp = (*(Free.begin())).second;
    }
    x = x_tmp;
    y = y_tmp;
    free_f--;
}

void EnemysField::shoot(int x, int y){
    Field[y*10 + x] = 1;
}

void EnemysField::round_right(int x, int y, int size){
    if(x < 9) {
        x++;
        Field[10 * y + x] = 1;
        if(x - size > 0)
            size = x - size - 1;
        else
            size = x - size;
    } else
        size = x - size;
    for (; x >= size; x--) {
        if (y > 0)
            Field[10 * (y - 1) + x] = 1;
        if (y < 9)
            Field[10 * (y + 1) + x] = 1;
    }
    Field[10*y + x + 1] = 1;
}
void EnemysField::round_left(int x, int y, int size){
    if (x > 0) {
        x--;
        Field[10 * y + x] = 1;
        if (x + size < 9)
            size = x + size + 1;
        else
            size = x + size;
    } else
        size = x + size;
    for (; x <= size; x++){
        if (y > 0)
            Field[10 * (y - 1) + x] = 1;
        if (y < 9)
            Field[10 * (y + 1) + x] = 1;
    }
    Field[10*y + x - 1] = 1;
}
void EnemysField::round_top(int x, int y, int size){
    if (y > 0) {
        y--;
        Field[10 * y + x] = 1;
        if (y + size < 9)
            size = y + size + 1;
        else
            size = y + size;
    } else
        size = y + size;
    for(; y <= size; y++){
        if (x > 0)
            Field[10 * y + x - 1] = 1;
        if (x < 9)
            Field[10 * y + x + 1] = 1;
    }
    Field[10*(y-1) + x] = 1;
}
void EnemysField::round_bottom(int x, int y, int size){
    if (y < 9) {
        y++;
        Field[10 * y + x] = 1;
        if (y - size > 0)
            size = y - size - 1;
        else
            size = y - size;
    } else
        size = y - size;
    for (; y >= size; y--){
        if (x > 0)
            Field[10 * y + x - 1] = 1;
        if (x < 9)
            Field[10 * y + x + 1] = 1;
    }
    Field[10*(y+1) + x] = 1;
}

void EnemysField::round_one(int x, int y) {
    if(y < 9)
        Field[10*(y + 1) + x] = 1;
    if(y > 0)
        Field[10*(y - 1) + x] = 1;
    if (x < 9)
        Field[10*y + x + 1] = 1;
    if (x > 0)
        Field[10*y + x - 1] = 1;

    if (x > 0 && y > 0)
        Field[10*(y-1) + x - 1] = 1;
    if(x > 0 && y < 9)
        Field[10*(y+1) + x - 1] = 1;
    if(x < 9 && y < 9)
        Field[10 * (y + 1) + x + 1] = 1;
    if(x < 9 && y > 0)
        Field[10*(y - 1) + x + 1] = 1;
}

int EnemysField::can_soot (int x, int y){
    if(Field[10 * y + x] == 1)
        return 0;
    else
        return 1;
}