#include "header.h"

MyField::MyField(): Field(100, 0){
    srand(std::time(NULL));
    top = false;
    bottom = false;
    left = false;
    right = false;
    ArrangeField();
}

void MyField::ArrangeField(){
    ArrangeBattleship();
    ArrangeCruiser();
    ArrangeDestroyer();
    ArrangeBoat();
}

void MyField::ArrangeBattleship(){
    int tmp, l, i;
    tmp = rand() % 4;
    l = rand() % 7;
    switch (tmp) {
        case 0:
            for (i = l; i < l + 4; i++) {
                Field[i * 10] = 1;
                Field[i * 10 + 1] = 2;
            }
            if (l - 1 >= 0) {
                Field[(l - 1) * 10] = 2;
                Field[(l - 1) * 10 + 1] = 2;
            }
            if (l + 4 != 10) {
                Field[(l + 4) * 10] = 2;
                Field[(l + 4) * 10 + 1] = 2;
            }
            left = true;
            break;
        case 1:
            for (i = l; i < l + 4; i++) {
                Field[i * 10 + 9] = 1;
                Field[i * 10 + 8] = 2;
            }
            if (l - 1 >= 0) {
                Field[(l - 1) * 10 + 9] = 2;
                Field[(l - 1) * 10 + 8] = 2;
            }
            if (l + 4 != 10) {
                Field[(l + 4) * 10 + 9] = 2;
                Field[(l + 4) * 10 + 8] = 2;
            }
            right = true;
            break;
        case 2:
            for (i = l; i < l + 4; i++) {
                Field[i] = 1;
                Field[10 + i] = 2;
            }
            if (l - 1 >= 0) {
                Field[l - 1] = 2;
                Field[10 + l - 1] = 2;
            }
            if (l + 4 != 10) {
                Field[l + 4] = 2;
                Field[10 + l + 4] = 2;
            }
            top = true;
            break;
        case 3:
            for (i = l; i < l + 4; i++) {
                Field[90 + i] = 1;
                Field[80 + i] = 2;
            }
            if (l - 1 >= 0) {
                Field[90 + l - 1] = 2;
                Field[80 + l - 1] = 2;
            }
            if (l + 4 != 10) {
                Field[90 + l + 4] = 2;
                Field[80 + l + 4] = 2;
            }
            bottom = true;
            break;
    }
}


void MyField::ArrangeCruiser(){
    int tmp, i, j, x, y, n = 0, t = 0;
    bool done = false, err = false;
    if(bottom)
        tmp = 2;
    if(top)
        tmp = 3;
    if(left)
        tmp = 1;
    if(right)
        tmp = 0;
    x = rand() % 4;
    y = x + 4 + (rand() % (10 - x - 4 - 2));
    while(!done) {
        switch (tmp) {
            case 0:
                for (i = x; i < x + 3; i++) {
                    Field[i * 10] = 1;
                    Field[i * 10 + 1] = 2;
                }
                if (x - 1 >= 0) {
                    Field[(x - 1) * 10] = 2;
                    Field[(x - 1) * 10 + 1] = 2;
                }
                if (x + 3 != 10) {
                    Field[(x + 3) * 10] = 2;
                    Field[(x + 3) * 10 + 1] = 2;
                }
                for (i = y; i < y + 3; i++) {
                    Field[i * 10] = 1;
                    Field[i * 10 + 1] = 2;
                }
                if (y - 1 >= 0) {
                    Field[(y - 1) * 10] = 2;
                    Field[(y - 1) * 10 + 1] = 2;
                }
                if (y + 3 != 10) {
                    Field[(y + 3) * 10] = 2;
                    Field[(y + 3) * 10 + 1] = 2;
                }
                left = true;
                done = true;
                break;
            case 1:
                for (i = x; i < x + 3; i++) {
                    Field[i * 10 + 9] = 1;
                    Field[i * 10 + 8] = 2;
                }
                if (x - 1 >= 0) {
                    Field[(x - 1) * 10 + 9] = 2;
                    Field[(x - 1) * 10 + 8] = 2;
                }
                if (x + 3 != 10) {
                    Field[(x + 3) * 10 + 9] = 2;
                    Field[(x + 3) * 10 + 8] = 2;
                }
                for (i = y; i < y + 3; i++) {
                    Field[i * 10 + 9] = 1;
                    Field[i * 10 + 8] = 2;
                }
                if (y - 1 >= 0) {
                    Field[(y - 1) * 10 + 9] = 2;
                    Field[(y - 1) * 10 + 8] = 2;
                }
                if (y + 3 != 10) {
                    Field[(y + 3) * 10 + 9] = 2;
                    Field[(y + 3) * 10 + 8] = 2;
                }
                right = true;
                done = true;
                break;
            case 2:
                for (i = x; i < x + 3; i++) {
                    Field[i] = 1;
                    Field[10 + i] = 2;
                }
                if (x - 1 >= 0) {
                    Field[x - 1] = 2;
                    Field[10 + x - 1] = 2;
                }
                if (x + 3 != 10) {
                    Field[x + 3] = 2;
                    Field[10 + x + 3] = 2;
                }
                for (i = y; i < y + 3; i++) {
                    Field[i] = 1;
                    Field[10 + i] = 2;
                }
                if (y - 1 >= 0) {
                    Field[y - 1] = 2;
                    Field[10 + y - 1] = 2;
                }
                if (y + 3 != 10) {
                    Field[y + 3] = 2;
                    Field[10 + y + 3] = 2;
                }
                top = true;
                done = true;
                break;
            case 3:
                for (i = x; i < x + 3; i++) {
                    Field[90 + i] = 1;
                    Field[80 + i] = 2;
                }
                if (x - 1 >= 0) {
                    Field[90 + x - 1] = 2;
                    Field[80 + x - 1] = 2;
                }
                if (x + 3 != 10) {
                    Field[90 + x + 3] = 2;
                    Field[80 + x + 3] = 2;
                }
                for (i = y; i < y + 3; i++) {
                    Field[90 + i] = 1;
                    Field[80 + i] = 2;
                }
                if (y - 1 >= 0) {
                    Field[90 + y - 1] = 2;
                    Field[80 + y - 1] = 2;
                }
                if (y + 3 != 10) {
                    Field[90 + y + 3] = 2;
                    Field[80 + y + 3] = 2;
                }
                bottom = true;
                done = true;
                break;
        }
    }
}

void MyField::ArrangeDestroyer(){
    int tmp, i, x, y;
    int t = rand() % 2;
    int line1 = -1, line2 = -1;
    bool done = false;
    if(!top)
        line1 = 2;
    if(!bottom)
        if (line1 == -1)
            line1 = 3;
        else
            line2 = 3;
    if(!left)
        if (line1 == -1)
            line1 = 0;
        else
            line2 = 0;
    if(!right)
        if (line1 == -1)
            line1 = 1;
        else
            line2 = 1;
    if(t) {
        int t = line1;
        line1 = line2;
        line2 = t;
    }
    tmp = line1;
    while (!done) {
        x = rand() % 9;
        switch (tmp) {
            case 0:
                if (x == 0) {
                    if (Field[x * 10] != 0 || Field[(x + 1) * 10] != 0 || Field[(x + 2) * 10] == 1 || Field[x * 10 + 1] == 1)
                        break;
                } else{
                    if (x == 8){
                        if(Field[(x - 1) * 10] == 1 || Field[x * 10] != 0 && Field[(x + 1) * 10] != 0 || Field[(x + 1) * 10 + 1] == 1)
                            break;
                    }
                    else {
                        if (Field[(x - 1) * 10 + 1] == 1 || Field[(x - 1) * 10] == 1 || Field[x * 10] != 0 || Field[(x + 1) * 10] != 0 ||
                            Field[(x + 2) * 10] == 1 || Field[(x + 2) * 10 + 1] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[i * 10] = 1;
                    Field[i * 10 + 1] = 2;
                }
                if (x - 1 >= 0) {
                    Field[(x - 1) * 10] = 2;
                    Field[(x - 1) * 10 + 1] = 2;
                }
                if (x + 2 != 10) {
                    Field[(x + 2) * 10] = 2;
                    Field[(x + 2) * 10 + 1] = 2;
                }
                done = true;
                break;
            case 1:
                if (x == 0) {
                    if (Field[x * 10 + 9] != 0 || Field[(x + 1) * 10 + 9] != 0 || Field[(x + 2) * 10 + 9] == 1 || Field[x * 10 + 8] == 1)
                        break;
                } else{
                    if(x == 8){
                        if(Field[(x - 1) * 10 + 9] == 1 || Field[x * 10 + 9] != 0 || Field[(x + 1) * 10 + 9] != 0 || Field[(x + 1) * 10 + 8] == 1)
                            break;
                    }else {
                        if (Field[(x - 1) * 10 + 8] == 1 || Field[(x - 1) * 10 + 9] == 1 || Field[x * 10 + 9] != 0 || Field[(x + 1) * 10 + 9] != 0 ||
                            Field[(x + 2) * 10 + 9] == 1 || Field[(x + 2) * 10 + 8] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[i * 10 + 9] = 1;
                    Field[i * 10 + 8] = 2;
                }
                if (x - 1 >= 0) {
                    Field[(x - 1) * 10 + 9] = 2;
                    Field[(x - 1) * 10 + 8] = 2;
                }
                if (x + 2 != 10) {
                    Field[(x + 2) * 10 + 9] = 2;
                    Field[(x + 2) * 10 + 8] = 2;
                }
                done = true;
                break;
            case 2:
                if (x == 0) {
                    if (Field[x] != 0 || Field[x + 1] != 0 || Field[x + 2] == 1 || Field[10 + x] == 1)
                        break;
                } else{
                    if(x == 8){
                        if (Field[x - 1] == 1 || Field[x] != 0 || Field[x + 1] != 0 || Field[10 + x + 1] == 1)
                            break;
                    }else {
                        if (Field[10 + x - 1] == 1 || Field[x - 1] == 1 || Field[x] != 0 || Field[x + 1] != 0 &&
                                Field[x + 2] == 1 || Field[10 + x + 2] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[i] = 1;
                    Field[10 + i] = 2;
                }
                if (x - 1 >= 0) {
                    Field[x - 1] = 2;
                    Field[10 + x - 1] = 2;
                }
                if (x + 2 != 10) {
                    Field[x + 2] = 2;
                    Field[10 + x + 2] = 2;
                }
                done = true;
                break;
            case 3:
                if (x == 0) {
                    if (Field[80 + x] == 1 || Field[90 + x] != 0 || Field[90 + x + 1] != 0 || Field[90 + x + 2] == 1)
                        break;
                } else{
                    if (x == 8){
                        if (Field[90 + x] != 0 || Field[90 + x + 1] != 0 || Field[80 + x + 1] == 1)
                            break;
                    } else {
                        if (Field[80 + x - 1] == 1 || Field[90 + x - 1] == 1 || Field[90 + x] != 0 || Field[90 + x + 1] != 0 ||
                            Field[90 + x + 2] == 1 || Field[80 + x + 2] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[90 + i] = 1;
                    Field[80 + i] = 2;
                }
                if (x - 1 >= 0) {
                    Field[90 + x - 1] = 2;
                    Field[80 + x - 1] = 2;
                }
                if (x + 2 != 10) {
                    Field[90 + x + 2] = 2;
                    Field[80 + x + 2] = 2;
                }
                done = true;
                break;
        }
    }
    done = false;
    tmp = line2;
    while(!done) {
        x = rand() % 6;
        y = x + 3 + (rand() % (10 - x - 3 -1));
        switch (tmp) {
            case 0:
                if (x == 0) {
                    if (Field[x * 10] != 0 || Field[(x + 1) * 10] != 0 || Field[(x + 2) * 10] == 1 || Field[x * 10 + 1] == 1)
                        break;
                } else{
                    if (x == 8){
                        if(Field[(x - 1) * 10] == 1 || Field[x * 10] != 0 || Field[(x + 1) * 10] != 0 || Field[(x + 1) * 10 + 1] == 1)
                            break;
                    }
                    else {
                        if (Field[(x - 1) * 10 + 1] == 1 || Field[(x - 1) * 10] == 1 || Field[x * 10] != 0 || Field[(x + 1) * 10] != 0 ||
                            Field[(x + 2) * 10] == 1 || Field[(x + 2) * 10 + 1] == 1)
                            break;
                    }
                }
                if (y == 0) {
                    if (Field[y * 10] != 0 || Field[(y + 1) * 10] != 0 || Field[(y + 2) * 10] == 1 || Field[y * 10 + 1] == 1)
                        break;
                } else{
                    if (y == 8){
                        if(Field[(y - 1) * 10] == 1 || Field[y * 10] != 0 || Field[(y + 1) * 10] != 0 || Field[(y + 1) * 10 + 1] == 1)
                            break;
                    }
                    else {
                        if (Field[(y - 1) * 10 + 1] == 1 || Field[(y - 1) * 10] == 1 || Field[y * 10] != 0 || Field[(y + 1) * 10] != 0 ||
                            Field[(y + 2) * 10] == 1 || Field[(y + 2) * 10 + 1] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[i * 10] = 1;
                    Field[i * 10 + 1] = 2;
                }
                if (x - 1 >= 0) {
                    Field[(x - 1) * 10] = 2;
                    Field[(x - 1) * 10 + 1] = 2;
                }
                if (x + 2 != 10) {
                    Field[(x + 2) * 10] = 2;
                    Field[(x + 2) * 10 + 1] = 2;
                }
                for (i = y; i < y + 2; i++) {
                    Field[i * 10] = 1;
                    Field[i * 10 + 1] = 2;
                }
                if (y - 1 >= 0) {
                    Field[(y - 1) * 10] = 2;
                    Field[(y - 1) * 10 + 1] = 2;
                }
                if (y + 2 != 10) {
                    Field[(y + 2) * 10] = 2;
                    Field[(y + 2) * 10 + 1] = 2;
                }
                done = true;
                break;
            case 1:
                if (x == 0) {
                    if (Field[x * 10 + 9] != 0 || Field[(x + 1) * 10 + 9] != 0 || Field[(x + 2) * 10 + 9] == 1 || Field[x * 10 + 8] == 1)
                        break;
                } else{
                    if(x == 8){
                        if(Field[(x - 1) * 10 + 9] == 1 || Field[x * 10 + 9] != 0 || Field[(x + 1) * 10 + 9] != 0 || Field[(x + 1) * 10 + 8] == 1)
                            break;
                    }else {
                        if (Field[(x - 1) * 10 + 8] == 1 || Field[(x - 1) * 10 + 9] == 1 || Field[x * 10 + 9] != 0 || Field[(x + 1) * 10 + 9] != 0 ||
                            Field[(x + 2) * 10 + 9] == 1 || Field[(x + 2) * 10 + 8] == 1)
                            break;
                    }
                }
                if (y == 0) {
                    if (Field[y * 10 + 9] != 0 || Field[(y + 1) * 10 + 9] != 0 || Field[(y + 2) * 10 + 9] == 1 || Field[y * 10 + 8] == 1)
                        break;
                } else{
                    if(y == 8){
                        if(Field[(y - 1) * 10 + 9] == 1 || Field[y * 10 + 9] != 0 || Field[(y + 1) * 10 + 9] != 0 || Field[(y + 1) * 10 + 8] == 1)
                            break;
                    }else {
                        if (Field[(y - 1) * 10 + 8] == 1 || Field[(y - 1) * 10 + 9] == 1 || Field[y * 10 + 9] != 0 || Field[(y + 1) * 10 + 9] != 0 ||
                            Field[(y + 2) * 10 + 9] == 1 || Field[(y + 2) * 10 + 8] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[i * 10 + 9] = 1;
                    Field[i * 10 + 8] = 2;
                }
                if (x - 1 >= 0) {
                    Field[(x - 1) * 10 + 9] = 2;
                    Field[(x - 1) * 10 + 8] = 2;
                }
                if (x + 2 != 10) {
                    Field[(x + 2) * 10 + 9] = 2;
                    Field[(x + 2) * 10 + 8] = 2;
                }
                for (i = y; i < y + 2; i++) {
                    Field[i * 10 + 9] = 1;
                    Field[i * 10 + 8] = 2;
                }
                if (y - 1 >= 0) {
                    Field[(y - 1) * 10 + 9] = 2;
                    Field[(y - 1) * 10 + 8] = 2;
                }
                if (y + 2 != 10) {
                    Field[(y + 2) * 10 + 9] = 2;
                    Field[(y + 2) * 10 + 8] = 2;
                }
                done = true;
                break;
            case 2:
                if (x == 0) {
                    if (Field[x] != 0 || Field[x + 1] != 0 || Field[x + 2] == 1 || Field[10 + x] == 1)
                        break;
                } else{
                    if(x == 8){
                        if (Field[x - 1] == 1 || Field[x] != 0 || Field[x + 1] != 0 || Field[10 + x + 1] == 1)
                            break;
                    }else {
                        if (Field[10 + x - 1] == 1 || Field[x - 1] == 1 || Field[x] != 0 || Field[x + 1] != 0 ||
                            Field[x + 2] == 1 || Field[10 + x + 2] == 1)
                            break;
                    }
                }
                if (y == 0) {
                    if (Field[y] != 0 || Field[y + 1] != 0 || Field[y + 2] == 1 || Field[10 + y] == 1)
                        break;
                } else{
                    if(y == 8){
                        if (Field[y - 1] == 1 || Field[y] != 0 || Field[y + 1] != 0 || Field[10 + y + 1] == 1)
                            break;
                    }else {
                        if (Field[10 + y - 1] == 1 || Field[y - 1] == 1 || Field[y] != 0 || Field[y + 1] != 0 ||
                            Field[y + 2] == 1 || Field[10 + y + 2] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[i] = 1;
                    Field[10 + i] = 2;
                }
                if (x - 1 >= 0) {
                    Field[x - 1] = 2;
                    Field[10 + x - 1] = 2;
                }
                if (x + 2 != 10) {
                    Field[x + 2] = 2;
                    Field[10 + x + 2] = 2;
                }
                for (i = y; i < y + 2; i++) {
                    Field[i] = 1;
                    Field[10 + i] = 2;
                }
                if (y - 1 >= 0) {
                    Field[y - 1] = 2;
                    Field[10 + y - 1] = 2;
                }
                if (y + 2 != 10) {
                    Field[y + 2] = 2;
                    Field[10 + y + 2] = 2;
                }
                done = true;
                break;
            case 3:
                if (x == 0) {
                    if (Field[80 + x] == 1 || Field[90 + x] != 0 || Field[90 + x + 1] != 0 || Field[90 + x + 2] == 1)
                        break;
                } else{
                    if (x == 8){
                        if (Field[90 + x] != 0 || Field[90 + x + 1] != 0 || Field[80 + x + 1] == 1)
                            break;
                    } else {
                        if (Field[80 + x - 1] == 1 || Field[90 + x - 1] == 1 || Field[90 + x] != 0 || Field[90 + x + 1] != 0 ||
                            Field[90 + x + 2] == 1 || Field[80 + x + 2] == 1)
                            break;
                    }
                }
                if (y == 0) {
                    if (Field[80 + y] == 1 || Field[90 + y] != 0 || Field[90 + y + 1] != 0 || Field[90 + y + 2] == 1)
                        break;
                } else{
                    if (y == 8){
                        if (Field[90 + y] != 0 || Field[90 + y + 1] != 0 || Field[80 + y + 1] == 1)
                            break;
                    } else {
                        if (Field[80 + y - 1] == 1 || Field[90 + y - 1] == 1 || Field[90 + y] != 0 || Field[90 + y + 1] != 0 ||
                            Field[90 + y + 2] == 1 || Field[80 + y + 2] == 1)
                            break;
                    }
                }
                for (i = x; i < x + 2; i++) {
                    Field[90 + i] = 1;
                    Field[80 + i] = 2;
                }
                if (x - 1 >= 0) {
                    Field[90 + x - 1] = 2;
                    Field[80 + x - 1] = 2;
                }
                if (x + 2 != 10) {
                    Field[90 + x + 2] = 2;
                    Field[80 + x + 2] = 2;
                }
                for (i = y; i < y + 2; i++) {
                    Field[90 + i] = 1;
                    Field[80 + i] = 2;
                }
                if (y - 1 >= 0) {
                    Field[90 + y - 1] = 2;
                    Field[80 + y - 1] = 2;
                }
                if (y + 2 != 10) {
                    Field[90 + y + 2] = 2;
                    Field[80 + y + 2] = 2;
                }
                done = true;
                break;
        }
    }
}

void MyField::ArrangeBoat(){
    bool done = false;
    int x, y;
    for (int k = 0; k < 4; k++) {
        while (!done) {
            x = rand() % 10;
            y = rand() % 10;
            if (x > 1 && x < 8 && y > 1 && y < 8 && Field[x * 10 + y] == 0 &&
                Field[x * 10 + y + 1] != 1 && Field[(x + 1) * 10 + y + 1] != 1 &&
                Field[(x + 1) * 10 + y] != 1 && Field[(x + 1) * 10 + y - 1] != 1 &&
                Field[x * 10 + y - 1] != 1 && Field[(x - 1) * 10 + y - 1] != 1 &&
                Field[(x - 1) * 10 + y] != 1 && Field[(x - 1) * 10 + y + 1] != 1) {
                for (int i = x - 1; i < x + 2; i++)
                    for (int j = y - 1; j < y + 2; j++)
                        Field[i * 10 + j] = 2;
                Field[x * 10 + y] = 1;
                done = true;
            }
        }
        done = false;
    }
}

void  MyField::sendMyField(){
    for(int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++)
            if (Field[i*10 + j] == 1)
                std::cout << Field[i*10 + j];
            else
                std::cout << ".";
        std::cout << std::endl;
    }
}