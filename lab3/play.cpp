#include "header.h"

Game::Game(){
    hit = false;
    kill = false;
    right = false;
    left = false;
    top = false;
    bottom = false;
    try_left = false;
    try_right = false;
    try_top = false;
    try_bottom = false;
}

int Game::play() {
    std::string command;
    std::getline(std::cin, command);
    int x, y;
    char X;
    if (command == "Arrange!")
        myField.sendMyField();
    else return 2;
    std::getline(std::cin, command);
    if(command != "Shoot!")
        return 3;
    while (command != "Win!" || command != "Lose!"){
        if(kill){
            enemysField.get_coordinates(x, y);
            enemysField.shoot(x,y);
            X = static_cast<char>(x + 65);
            std::cout << X << " " << y << std::endl;
            std::getline(std::cin, command);
            if(command == "Hit") {
                hit = true;
                kill = false;
            }
            if(command == "Kill") {
                kill = true;
                hit = false;
                enemysField.round_one(x, y);
            }
            if(command == "Miss"){
                kill = false;
                hit = false;
            }
            if (command == "Win!" || command == "Lose!")
                break;
        }else {
            if (!hit) {
                enemysField.get_coordinates(x, y);
                X = static_cast<char>(x + 65);
                std::cout << X << " " << y << std::endl;
                enemysField.shoot(x, y);
                std::getline(std::cin, command);
                if (command == "Hit")
                    hit = true;
                if (command == "Kill") {
                    kill = true;
                    enemysField.round_one(x, y);
                }
                if (command == "Win!" || command == "Lose!")
                    break;
            } else {
                int s = 2;
                while (right && command != "Miss") {
                    if (x < 9)
                        x++;
                    else{
                        right = false;
                        left = true;
                        x = x - s + 1;
                        break;
                    }
                    if(enemysField.can_soot(x,y)) {
                        X = static_cast<char>(x + 65);
                        std::cout << X << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command != "Miss")
                            s++;
                        if (command == "Kill") {
                            enemysField.round_right(x, y, s);
                            right = false;
                            hit = false;
                            kill = true;
                            try_right = false;
                            try_top = false;
                            try_bottom = false;
                            try_left = false;
                            s = 2;
                        } else if (command == "Miss") {
                            right = false;
                            left = true;
                            x = x - s;
                        }
                        if(command == "Hit" && x == 9){
                            right = false;
                            left = true;
                            x = x - s + 1;
                            break;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    } else{
                        right = false;
                        left = true;
                        x = x - s;
                        break;
                    }
                }
                if (command == "Win!" || command == "Lose!")
                    break;
                while (left && command != "Miss") {
                    if( x > 0 ) {
                        std::cerr << x << " " << y << std::endl;
                        x--;
                        std::cerr << x << " " << y << std::endl;
                    }
                    else{
                        left = false;
                        right = true;
                        x = x + s - 1;
                        break;
                    }
                    if(enemysField.can_soot(x,y)) {
                        X = static_cast<char>(x + 65);
                        std::cout << X << " " << y << std::endl;
                        std::cerr << x << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command != "Miss")
                            s++;
                        if (command == "Kill") {
                            enemysField.round_left(x, y, s);
                            left = false;
                            hit = false;
                            kill = true;
                            try_right = false;
                            try_top = false;
                            try_bottom = false;
                            try_left = false;
                            s = 2;
                        } else if (command == "Miss") {
                            left = false;
                            right = true;
                            x = x + s;
                        }
                        if(command == "Hit" && x == 0){
                            left = false;
                            right = true;
                            x = x + s - 1;
                            break;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    } else{
                        left = false;
                        right = true;
                        x = x + s;
                        break;
                    }
                }
                if (command == "Win!" || command == "Lose!")
                    break;
                while (top && command != "Miss") {
                    if (y > 0)
                        y--;
                    else {
                        top = false;
                        bottom = true;
                        y = y + s - 1;
                        break;
                    }
                    if(enemysField.can_soot(x,y)) {
                        X = static_cast<char>(x + 65);
                        std::cout << X << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command != "Miss")
                            s++;
                        if (command == "Kill") {
                            enemysField.round_top(x, y, s);
                            top = false;
                            hit = false;
                            kill = true;
                            try_right = false;
                            try_top = false;
                            try_bottom = false;
                            try_left = false;
                            s = 2;
                        } else if (command == "Miss") {
                            top = false;
                            bottom = true;
                            y = y + s;
                        }
                        if(command == "Hit" && y == 0){
                            bottom = true;
                            top = false;
                            y = y + s - 1;
                            break;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    } else{
                        top = false;
                        bottom = true;
                        y = y + s;
                        break;
                    }
                }
                if (command == "Win!" || command == "Lose!")
                    break;
                while (bottom && command != "Miss") {
                    if(y < 9)
                        y++;
                    else {
                        bottom = false;
                        top = true;
                        y = y - s + 1;
                        break;
                    }
                    if(enemysField.can_soot(x,y)) {
                        X = static_cast<char>(x + 65);
                        std::cout << X << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command != "Miss")
                            s++;
                        if (command == "Kill") {
                            enemysField.round_bottom(x, y, s);
                            bottom = false;
                            hit = false;
                            kill = true;
                            try_right = false;
                            try_top = false;
                            try_bottom = false;
                            try_left = false;
                            s = 2;
                        }
                        if (command == "Miss") {
                            bottom = false;
                            top = true;
                            y = y - s;
                        }
                        if(command == "Hit" && y == 9){
                            bottom = false;
                            top = true;
                            y = y - s + 1;
                            break;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    } else{
                        bottom = false;
                        top = true;
                        y = y - s;
                        break;
                    }
                }
                if (command == "Win!" || command == "Lose!")
                    break;
                if (!try_bottom && y < 9 && (command == "Hit" || command == "Shoot!")) {
                    y++;
                    X = static_cast<char>(x + 65);
                    if(enemysField.can_soot(x,y)) {
                        std::cout << X << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command == "Hit") {
                            bottom = true;
                            try_top = true;
                            try_left = true;
                            try_right = true;
                            try_bottom = true;
                        }
                        if (command == "Kill") {
                            kill = true;
                            try_bottom = false;
                            try_top = false;
                            try_right = false;
                            try_left = false;
                            enemysField.round_bottom(x, y, 2);
                        }
                        if (command == "Miss") {
                            try_bottom = true;
                            y--;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    } else{
                        try_bottom = true;
                        y--;
                    }
                    if (command == "Win!" || command == "Lose!")
                        break;
                }
                if (!try_top && y > 0 && (command == "Hit" || command == "Shoot!")) {
                    y--;
                    X = static_cast<char>(x + 65);
                    if (enemysField.can_soot(x,y)) {
                        std::cout << X << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command == "Hit") {
                            top = true;
                            try_top = true;
                            try_left = true;
                            try_right = true;
                            try_bottom = true;
                        }
                        if (command == "Kill") {
                            kill = true;
                            try_bottom = false;
                            try_top = false;
                            try_right = false;
                            try_left = false;
                            enemysField.round_top(x, y, 2);
                        }
                        if (command == "Miss") {
                            try_top = true;
                            y++;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    }else{
                        try_top = true;
                        y++;
                    }
                    if (command == "Win!" || command == "Lose!")
                        break;
                }
                if (!try_left && x > 0 && (command == "Hit" || command == "Shoot!")) {
                    x--;
                    X = static_cast<char>(x + 65);
                    if(enemysField.can_soot(x,y)) {
                        std::cout << X << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command == "Hit") {
                            left = true;
                            try_top = true;
                            try_left = true;
                            try_right = true;
                            try_bottom = true;
                        }
                        if (command == "Kill") {
                            kill = true;
                            try_bottom = false;
                            try_top = false;
                            try_right = false;
                            try_left = false;
                            enemysField.round_left(x, y, 2);
                        }
                        if (command == "Miss") {
                            try_left = true;
                            x++;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    } else{
                        try_left = true;
                        x++;
                    }
                    if (command == "Win!" || command == "Lose!")
                        break;
                }
                if (!try_right && x < 9 && (command == "Hit" || command == "Shoot!")) {
                    x++;
                    X = static_cast<char>(x + 65);
                    if (enemysField.can_soot(x, y)) {
                        std::cout << X << " " << y << std::endl;
                        enemysField.shoot(x, y);
                        std::getline(std::cin, command);
                        if (command == "Hit") {
                            right = true;
                            try_top = true;
                            try_left = true;
                            try_right = true;
                            try_bottom = true;
                        }
                        if (command == "Kill") {
                            kill = true;
                            try_bottom = false;
                            try_top = false;
                            try_right = false;
                            try_left = false;
                            enemysField.round_right(x, y, 2);
                        }
                        if (command == "Miss") {
                            try_right = true;
                            x--;
                        }
                        if (command == "Win!" || command == "Lose!")
                            break;
                    }
                    else{
                        try_right = true;
                        x--;
                    }
                    if (command == "Win!" || command == "Lose!")
                        break;
                }
            }
        }
        if (command == "Win!" || command == "Lose!")
            break;
        if(command == "Miss"){
            while(command != "Shoot!") {
                std::getline(std::cin, command);
                if (command == "Win!" || command == "Lose!")
                    break;
            }
            if (command == "Win!" || command == "Lose!")
                break;
        }
    }
}

