#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <ctime>
#include <list>

class MyField{
private:
    std::vector<int> Field;
    bool top;
    bool bottom;
    bool left;
    bool right;
    void ArrangeField();
    void ArrangeBattleship();
    void ArrangeCruiser();
    void ArrangeDestroyer();
    void ArrangeBoat();
public:
    MyField();
    void sendMyField();
};

class EnemysField{
private:
    std::vector<int> Field;
    std::list<std::pair<int, int>> Free;
    int free_f;
public:
    EnemysField();
    void shoot(int x, int y);
    void get_coordinates(int &x, int &y);
    void round_right(int x, int y, int size);
    void round_left(int x, int y, int size);
    void round_top(int x, int y, int size);
    void round_bottom(int x, int y, int size);
    void round_one(int x, int y);
    int can_soot (int x, int y);
};

class Game{
private:
    MyField myField;
    EnemysField enemysField;
    bool right;
    bool left;
    bool top;
    bool bottom;
    bool hit;
    bool kill;
    bool try_left;
    bool try_right;
    bool try_top;
    bool try_bottom;
public:
    Game();
    int play();
};
