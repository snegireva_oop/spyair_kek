#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <vector>
#include <fstream>
#include <algorithm>
#include <list>
#include <sstream>
std::vector<std::pair<std::string, int>> mapinvector(const std::map<std::string,int> &my_map);
bool comp(const std::pair<std::string,int> &i, const std::pair<std::string, int> &j);
std::map<std::string, int> func(std::istream &f, int n);
