cmake_minimum_required(VERSION 3.6)
project(lab1_1_)

add_definitions(-DWINVER=0x5000)

include_directories(gtest-1.7.0/googletest/include gtest-1.7.0/googletest)

set(CMAKE_CXX_STANDARD 11)

add_subdirectory(gtest-1.7.0/googletest)

set(SOURCE_FILES main.cpp funcs.cpp header.h)
set(UNIT_TESTS testmain.cpp funcs.cpp)

add_executable(lab1_1_ ${SOURCE_FILES})
add_executable(Tests ${UNIT_TESTS})

target_link_libraries(Tests gtest)
