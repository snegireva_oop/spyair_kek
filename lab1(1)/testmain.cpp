#include "gtest/gtest.h"
#include "header.h"

TEST(test4mapinvector, T1){
    std::map<std::string,int> input = {
            {"we all", 1},
            {"yellow submarine", 3},
            {"leave in", 2},
            {"in a", 4}
    };
    std::vector<std::pair<std::string, int>> exp = {
            {"in a", 4},
            {"yellow submarine", 3},
            {"leave in", 2},
            {"we all", 1}
    };
    std::vector<std::pair<std::string,int>> output;
    output = mapinvector(input);
    EXPECT_EQ(output, exp);
}

TEST(test4func, T2) {
    std::istringstream is("we all live in a yellow submarine\n"
                                  "yellow submarine yellow submarine\n"
                                  "we all live in a yellow submarine\n"
                                  "yellow submarine yellow submarine");
    int input = 2;
    std::map<std::string, int> exp = {
            {"we all ",           2},
            {"all live ",         2},
            {"live in ",          2},
            {"in a ",             2},
            {"a yellow ",         2},
            {"yellow submarine ", 6},
            {"submarine yellow ", 4},
            {"submarine we ",     1}};
    auto output = func(is, input);
    EXPECT_EQ(output, exp);
}

int main(int argc, char *argv[]){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

