#include "header.h"

using namespace std;

vector<pair<string, int>> mapinvector(const map<string,int> &my_map){
    vector<pair<string, int>> my_v;
    for (auto it = my_map.begin(); it != my_map.end(); ++it){
        my_v.push_back(make_pair(it->first, it->second));
    }
    sort(my_v.begin(), my_v.end(), comp);
    return my_v;
};

bool comp(const pair<string,int> &i, const pair<string, int> &j){
    return (i.second > j.second);
}

map<string, int> func(istream &f, int n){
    int i = 0, k = 1;
    string word, phrase = "";
    list<string> tmp;
    map<string, int> my_map;
    while(!f.eof()){
        if (k) {
            while (i != n && !f.eof()) {
                f >> word;
                tmp.push_back(word);
                //phrase = phrase + word + " ";
                i++;
            }
            k = 0;
        } else{
            tmp.pop_front();
            f >> word;
            tmp.push_back(word);
        }
        for (auto it = tmp.begin(); it != tmp.end(); it++){
            phrase = phrase + *it + " ";
        }
        if (my_map.find(phrase) == my_map.end()){
            my_map.insert(pair<string, int>(phrase, 1));
        }
        else
            my_map[phrase]++;
        phrase = "";
        i = 0;
    }
    return my_map;
}

