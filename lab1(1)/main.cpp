#include "header.h"

char help[] = "This function count phrases \n\n"
        "Syntax: lab1 [-n][-m] ... [file_name] or [-]\n\n"
        "[-n] - Digit - number of words in phrase.\n "
        " Default: 2.\n"
        "[-m] - Digit - minimal number of repeats, in which the phrase should be shown on the screen.\n"
        " Default: 2.\n"
        "[-] - for keyboard reading.\n";

using namespace std;

int main(int argc, char *argv[]) {
    ifstream fcin;
    map<string, int> my_map;
    int n = 2, m = 2, i = 1;
    if (argc > 6 ){
        cout << help;
        return 0;
    }
    while (!strcmp(argv[i], "-n") || !strcmp(argv[i], "-m")){
        if (!strcmp(argv[i], "-n")){
            n = argv[i + 1][0] - '0';
            i += 2;
        }
        if (!strcmp(argv[i], "-m")){
            m = argv[i + 1][0] - '0';
            i += 2;
        }
    }
    fcin.open(argv[i], ifstream::in);
    if (strcmp(argv[i], "-") && !fcin.is_open()){
        cout << help;
        return 0;
    }
    if (fcin.is_open()){
        my_map = func(fcin, n);
        fcin.close();
    }
    else
        my_map = func(cin, n);
    vector<pair<string, int>> my_vect;
    my_vect = mapinvector(my_map);
    for (auto it = my_vect.begin(); it != my_vect. end(); ++it){
        if ((*it).second >= m)
            cout << (*it).first << ": " << (*it).second << endl;
    }
    return 0;
}