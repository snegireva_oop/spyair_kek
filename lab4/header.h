#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <map>
#include <fstream>
#include <typeinfo>
#include <stdexcept>
#include <list>
#include <algorithm>
#include <cstring>

class Any {
public:
    Any(): held_(nullptr){}
    template <typename T1>
    Any(const T1 &obj): held_(new smallholder<T1>(obj)){}
    Any(const Any &fo) = delete;
    Any(Any &&kek){
        held_ = kek.held_;
        kek.held_ = nullptr;
    }
    Any &operator=(Any &&kek){
        delete held_;
        held_ = kek.held_;
        kek.held_ = nullptr;
        return *this;
    }
    ~Any(){delete held_;}
    void get() const{
        if (held_ != nullptr)
            throw std::runtime_error("It was a bad scheme, don't you do it anymore:(");
    }
    template <typename T2>
    T2 get() const{
        if ((held_ == nullptr) || (typeid(T2) != held_->type_info())) {
            throw std::runtime_error("It was a bad scheme, don't you do it anymore:(");
        }

        return static_cast<smallholder<T2>*>(held_)->t;
    }
    template <typename T3>
    Any &operator=(const T3 &lol){
        Any tmp(lol);
        std::swap(tmp.held_,this->held_);
        return *this;
    };
private:
    struct holder{
        virtual const std::type_info &type_info() const = 0;
        virtual ~holder() = default;
    };
    template <typename T1>
    struct smallholder: holder{
        smallholder(const T1 &obj): t(obj){};
        const std::type_info &type_info() const override {
            return typeid(t);
        }
        T1 t;
    };

    holder *held_;
};



class Worker{
public:
    Worker() = default;
    virtual Any run(Any &obj) = 0;
};

class ReadFileWorker: public Worker{
private:
    std::string filename;
public:
    ReadFileWorker(std::string filename): filename(filename){};
    Any run(Any &obj) override;
};

class WriteFileWorker: public Worker{
private:
    std::string filename;
public:
    WriteFileWorker(std::string filename): filename(filename){};
    Any run(Any &obj) override;
};

class GrepWorker: public Worker{
private:
    std::string word;
public:
    GrepWorker(std::string word): word(word){};
    Any run(Any &obj) override;
};

class SortWorker: public Worker{
    Any run(Any &obj) override;
};

class ReplaceWorker: public Worker{
private:
    std::string what;
    std::string on;
public:
    ReplaceWorker(std::string what, std::string on): what(what), on(on){};
    Any run(Any &obj) override;
};

class DumpWorker: public Worker{
private:
    std::string filename;
public:
    DumpWorker(std::string filename): filename(filename){};
    Any run(Any &obj) override;
};


class WorkersManager{
private:
    std::map<int, Worker*> karta;
public:
    WorkersManager() = default;
    void addWorker(int id, Worker *worker);
    Any invokeWorker(int id, Any &param);
};

class Runner{
    Worker *first;
    Worker *last;
public:
    void setFirstWorker(Worker *sis);
    void setLastWorker(Worker * sis);
    Runner(): first(nullptr), last(nullptr){};
    void run(std::ifstream &in, WorkersManager &Manager);
};

class Parser{
public:
    Parser() = default;
    void parse(std::ifstream &in, WorkersManager &Manager, Runner &ruin);
};