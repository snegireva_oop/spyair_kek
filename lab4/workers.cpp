#include "header.h"

Any ReadFileWorker::run(Any &obj) {
    std::ifstream in(filename);
    obj.get();
    std::list<std::string> data;
    std::string line;
    while(!in.eof()){
        std::getline(in, line);
        data.push_back(line);
    }
    in.close();
    Any obj1(data);
    return obj1;
}

Any WriteFileWorker::run(Any &obj){
    std::ofstream out(filename);
    std::list<std::string> data = obj.get<std::list<std::string>>();
    while (!data.empty()){
        out << *(data.begin()) << std::endl;
        data.pop_front();
    }
    out.close();
    Any obj1;           //?????????????????
    return obj1;
}

Any GrepWorker::run(Any &obj){
    std::list<std::string> data = obj.get<std::list<std::string>>();
    auto it = data.begin();
    while (it != data.end()){
        if((*it).find(word) == -1)
            it = data.erase(it);
        else
            ++it;
    }
    Any obj1(data);
    return obj1;
}

Any ReplaceWorker::run(Any &obj){
    std::list<std::string> data = obj.get<std::list<std::string>>();
    auto it = data.begin();
    //size_t pos = 0, p;
    size_t size = what.size();
    while(it != data.end()){
        if((*it).find(what) == -1)
            it++;
        else {
            //size_t s = (*it).size();
            while ((*it).find(what) != -1) {
                //p = (*it).find(what, pos);
                //if (( (*it)[p] == ' ' || p == 0) && ((*it)[p + size] == ' ' || p + size == s)) {
                    (*it).replace((*it).find(what), size, on);
                //    pos = p + size2;
                //} else
                //    pos = p + size;
            }
           // it++;
        }
        //pos = 0;
    }
    Any obj1(data);
    return obj1;
}

Any SortWorker::run(Any &obj){
    std::list<std::string> data = obj.get<std::list<std::string>>();
    data.sort();
    Any obj1(data);
    return obj1;
}

Any DumpWorker::run(Any &obj){
    std::ofstream out(filename);
    std::list<std::string> data = obj.get<std::list<std::string>>();
    for (auto it = data.begin(); it != data.end(); ++it)
        out << (*it) << std::endl;
    return {data};
}