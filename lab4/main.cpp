#include "header.h"

using namespace std;

int main(int argc, char *argv[]) {
    Runner runner;
    if (argc > 2){
        for(int i = 1; i < argc; i++){
            if(strcmp(argv[i], "-i")){
                i++;
                runner.setFirstWorker(new ReadFileWorker(argv[i + 1]));
                i++;
            }
            if(strcmp((argv[i]),"-o")){
                i++;
                runner.setLastWorker(new WriteFileWorker(argv[i + 1]));
                i++;
            }
        }
    }
    try {
        ifstream in(argv[1]);
        WorkersManager manager;
        Parser parser;
        parser.parse(in, manager, runner);
        runner.run(in, manager);
        in.close();
    }
    catch (std::runtime_error &name){
        cout << name.what();
    }
    catch (...){
        cout << "Unexpected Err.";
    }
    return 0;
}