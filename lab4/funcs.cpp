#include "header.h"

void Parser::parse(std::ifstream &in, WorkersManager &Manager, Runner &ruin){
    std::string str;
    int id;
    bool first = false, last = false;
    in >> str;
    if (str != "desc")
        throw std:: runtime_error("Bad file format!!!");
    while (str != "csed"){
        if (str == "desc")
            in >> id;
        else
            id = atoi(str.c_str());
        in >> str;
        in >> str;
        if(str == "replace"){
            std::string on, what;
            in >> what >> on;
            ReplaceWorker *one = new ReplaceWorker(what, on);
            Manager.addWorker(id, one);
        }
        if(str == "readfile"){
            std::string filename;
            in >> filename;
            ReadFileWorker *one = new ReadFileWorker(filename);
            Manager.addWorker(id, one);
            first = true;
        }
        if(str == "writefile"){
            std::string filename;
            in >> filename;
            WriteFileWorker *one = new WriteFileWorker(filename);
            Manager.addWorker(id, one);
            last = true;
        }
        if(str == "grep"){
            std::string word;
            in >> word;
            GrepWorker *one = new GrepWorker(word);
            Manager.addWorker(id, one);
        }
        if(str == "sort"){
            SortWorker *one = new SortWorker();
            Manager.addWorker(id, one);
        }
        if(str == "dump"){
            std::string filename;
            in >> filename;
            DumpWorker *one = new DumpWorker(filename);
            Manager.addWorker(id, one);
        }
        in >> str;
    }
    if(first)
        ruin.setFirstWorker(nullptr);
    if(last)
        ruin.setLastWorker(nullptr);
}

void Runner::run(std::ifstream &in, WorkersManager &Manager){
    int no;
    std::string po;
    Any obj;
    if(first != nullptr)
        obj = first->run(obj);
    while(!in.eof()){
        in >> no;
        obj = Manager.invokeWorker(no, obj);
        if (!in.eof())
            in >> po;
    }
    if(last != nullptr)
        obj = last->run(obj);
    obj.get();
}

void WorkersManager::addWorker(int id, Worker *worker){
    if (karta.find(id) == karta.end())
        karta.insert(std::pair<int, Worker *>(id, worker));
    else
        throw std::runtime_error("Bad scheme:(((((");
}

Any WorkersManager::invokeWorker(int id, Any &param){

    auto it = karta.find(id);
    if (it == karta.end())
        throw std::runtime_error("Bad scheme:(((((");
    return ((it->second)->run(param));
}

void Runner::setFirstWorker(Worker *sis){
    first = sis;
}

void Runner::setLastWorker(Worker *sis){
    last = sis;
}