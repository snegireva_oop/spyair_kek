#include "header.h"

DateInterval::DateInterval (){
    years = 0;
    months = 0;
    days = 0;
    hours = 0;
    minutes = 0;
    seconds = 0;
}
DateInterval::DateInterval(int year, int mon, int day, int ho, int min, int sec){
    years = year;
    months = mon;
    days = day;
    hours = ho;
    minutes = min;
    seconds = sec;
}
DateInterval::DateInterval (const DateInterval &obj){
    years = obj.years;
    months = obj.months;
    days = obj.days;
    hours = obj.hours;
    minutes = obj.minutes;
    seconds = obj.seconds;
}
DateInterval::DateInterval(const calendar &date1, const calendar &date2){
    years = date2.get_year() - date1.get_year();
    months = static_cast<int>(date2.get_month()) - static_cast<int>(date1.get_month());
    days = date2.get_day() - date1.get_day();
    hours = date2.get_hour()  - date1.get_hour();
    minutes = date2.get_minutes() - date1.get_minutes();
    seconds = date2.get_second() - date1.get_second();
}

DateInterval& DateInterval::operator=(const DateInterval &data2) {
    years = data2.years;
    months = data2.months;
    days = data2.days;
    hours = data2.hours;
    minutes = data2.minutes;
    seconds = data2.seconds;
    return *this;
}

int DateInterval::get_YearInterval() const{
    return years;
}
int DateInterval::get_MonthInterval() const{
    return months;
}
int DateInterval::get_DayInterval() const{
    return days;
}
int DateInterval::get_HourInterval() const{
    return  hours;
}
int DateInterval::get_MinuteInterval() const{
    return minutes;
}
int DateInterval::get_SecondInterval() const{
    return seconds;
}