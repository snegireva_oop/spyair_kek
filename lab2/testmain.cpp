#include "gtest/gtest.h"
#include "header.h"

TEST(get_year, T1){
    calendar input(1234, 11, 11, 11, 11, 11);
    auto exp = input.get_year();
    int output = 1234;
    EXPECT_EQ(exp,output);
}

TEST(get_month, T2){
    calendar input(1234, 11, 11, 11, 11, 11);
    int exp = static_cast<int>(input.get_month());
    int output = 11;
    EXPECT_EQ(exp,output);
}

TEST(get_day, T3){
    calendar input(1234, 11, 11, 11, 11, 11);
    auto exp = input.get_day();
    int output = 11;
    EXPECT_EQ(exp,output);
}

TEST(get_hour, T4){
    calendar input(1234, 11, 11, 11, 11, 11);
    auto exp = input.get_hour();
    int output = 11;
    EXPECT_EQ(exp,output);
}

TEST(get_minutes, T5){
    calendar input(1234, 11, 11, 11, 11, 11);
    auto exp = input.get_minutes();
    int output = 11;
    EXPECT_EQ(exp,output);
}

TEST(get_second, T6){
    calendar input(1234, 11, 11, 11, 11, 11);
    auto exp = input.get_second();
    int output = 11;
    EXPECT_EQ(exp,output);
}

TEST(norm_data, T7){
    calendar input(1234, -123, 12,231,3211,12);
    calendar exp(1223, 9, 23, 20,31,12);
    EXPECT_EQ(input.get_second(),exp.get_second());
    EXPECT_EQ(input.get_minutes(),exp.get_minutes());
    EXPECT_EQ(input.get_hour(),exp.get_hour());
    EXPECT_EQ(input.get_day(),exp.get_day());
    EXPECT_EQ(static_cast<int>(input.get_month()), static_cast<int>(exp.get_month()));
    EXPECT_EQ(input.get_year(),exp.get_year());
}

TEST(calendar, T8){
    calendar input(1234, 1, 12, 1, 31, 12);
    calendar output(input);
    EXPECT_EQ(input.get_second(),output.get_second());
    EXPECT_EQ(input.get_minutes(),output.get_minutes());
    EXPECT_EQ(input.get_hour(),output.get_hour());
    EXPECT_EQ(input.get_day(),output.get_day());
    EXPECT_EQ(static_cast<int>(input.get_month()), static_cast<int>(output.get_month()));
    EXPECT_EQ(input.get_year(),output.get_year());
}

TEST(add_years, T9){
    calendar input(1234, 11, 12, 12, 12, 12);
    calendar exp(2780,11,12,12,12,12);
    calendar output = input.add_years(1546);
    EXPECT_EQ(exp.get_second(),output.get_second());
    EXPECT_EQ(exp.get_minutes(),output.get_minutes());
    EXPECT_EQ(exp.get_hour(),output.get_hour());
    EXPECT_EQ(exp.get_day(),output.get_day());
    EXPECT_EQ(static_cast<int>(exp.get_month()), static_cast<int>(output.get_month()));
    EXPECT_EQ(exp.get_year(),output.get_year());
}

TEST(add_months, T9){
    calendar input(1234, 11, 12, 12, 12, 12);
    calendar exp(1363,9,12,12,12,12);
    calendar output = input.add_months(1546);
    EXPECT_EQ(exp.get_second(),output.get_second());
    EXPECT_EQ(exp.get_minutes(),output.get_minutes());
    EXPECT_EQ(exp.get_hour(),output.get_hour());
    EXPECT_EQ(exp.get_day(),output.get_day());
    EXPECT_EQ(static_cast<int>(exp.get_month()), static_cast<int>(output.get_month()));
    EXPECT_EQ(exp.get_year(),output.get_year());
}

TEST(add_days, T9){
    calendar input(1234, 11, 12, 12, 12, 12);
    calendar exp(1239,2,5,12,12,12);
    calendar output = input.add_days(1546);
    EXPECT_EQ(exp.get_second(),output.get_second());
    EXPECT_EQ(exp.get_minutes(),output.get_minutes());
    EXPECT_EQ(exp.get_hour(),output.get_hour());
    EXPECT_EQ(exp.get_day(),output.get_day());
    EXPECT_EQ(static_cast<int>(exp.get_month()), static_cast<int>(output.get_month()));
    EXPECT_EQ(exp.get_year(),output.get_year());
}

TEST(add_hours, T9){
    calendar input(1234, 11, 12, 12, 12, 12);
    calendar exp(1235,1,15,22,12,12);
    calendar output = input.add_hours(1546);
    EXPECT_EQ(exp.get_second(),output.get_second());
    EXPECT_EQ(exp.get_minutes(),output.get_minutes());
    EXPECT_EQ(exp.get_hour(),output.get_hour());
    EXPECT_EQ(exp.get_day(),output.get_day());
    EXPECT_EQ(static_cast<int>(exp.get_month()), static_cast<int>(output.get_month()));
    EXPECT_EQ(exp.get_year(),output.get_year());
}

TEST(add_minutes, T9){
    calendar input(1234, 11, 12, 12, 12, 12);
    calendar exp(1234,11,13,13,58,12);
    calendar output = input.add_minutes(1546);
    EXPECT_EQ(exp.get_second(),output.get_second());
    EXPECT_EQ(exp.get_minutes(),output.get_minutes());
    EXPECT_EQ(exp.get_hour(),output.get_hour());
    EXPECT_EQ(exp.get_day(),output.get_day());
    EXPECT_EQ(static_cast<int>(exp.get_month()), static_cast<int>(output.get_month()));
    EXPECT_EQ(exp.get_year(),output.get_year());
}

TEST(add_seconds, T9){
    calendar input(1234, 11, 12, 12, 12, 12);
    calendar exp(1234,11,12,12,37,58);
    calendar output = input.add_seconds(1546);
    EXPECT_EQ(exp.get_second(),output.get_second());
    EXPECT_EQ(exp.get_minutes(),output.get_minutes());
    EXPECT_EQ(exp.get_hour(),output.get_hour());
    EXPECT_EQ(exp.get_day(),output.get_day());
    EXPECT_EQ(static_cast<int>(exp.get_month()), static_cast<int>(output.get_month()));
    EXPECT_EQ(exp.get_year(),output.get_year());
}


int main(int argc, char *argv[]){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}