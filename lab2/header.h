#include <iostream>
#include <ctime>

typedef unsigned  int u_int;
enum class month {JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AGU, SEP, OCT, NOV, DES};

class DateInterval;
std::ostream & operator<<(std::ostream& out, const month& m);
class calendar{
private:
    int years;
    month months;
    int days;
    int hours;
    int minutes;
    int seconds;
    void norm_seconds(int &min, int &sec);
    void norm_minutes(int &ho, int &min);
    void norm_hours(int &day, int &ho);
    void norm_data(int year, int mon, int day, int ho, int min, int sec);
    std::string getstr_mon(month mon) const ;
public:
    calendar();
    calendar(int year, int mon, int day, int ho, int min, int sec);;
    calendar(u_int year, month month, u_int day);
    calendar(u_int hour, u_int min, u_int sec);
    calendar(const calendar &obg);;
    calendar add_seconds(int second) const;;
    calendar add_minutes(int minute) const;;
    calendar add_hours(int hour) const;;
    calendar add_days(int day) const;;
    calendar add_months(int month) const;;
    calendar add_years(int year) const;;
    int get_year() const;;
    month get_month() const;;
    int get_day() const;;
    int get_hour() const;;
    int get_minutes() const;;
    int get_second() const;;
    calendar &operator=(const calendar &data2);
    std::string toString() const;
    calendar& operator++();
    calendar  operator++(int);
    calendar& operator--();
    calendar  operator--(int);
    DateInterval getInterval(const calendar &another) const;
    calendar addInterval (const DateInterval &amp) const;
    calendar  operator+ (const DateInterval &) const;
    calendar& operator+=(const DateInterval &);
    calendar  operator- (const DateInterval &) const;
    calendar& operator-=(const DateInterval &);
    std::string formatDate (std::string format);
    friend std::ostream& operator<<(std::ostream& os, const calendar &data);
};

std::ostream& operator<<(std::ostream& os, const calendar &data);

class DateInterval {
private:
    int years;
    int months;
    int days;
    int hours;
    int minutes;
    int seconds;
public:
    DateInterval ();
    DateInterval(int year, int mon, int day, int ho, int min, int sec);
    DateInterval(const DateInterval &obj);
    DateInterval(const calendar &date1, const calendar &date2);
    DateInterval &operator=(const DateInterval &data2);
    int get_YearInterval() const;
    int get_MonthInterval() const;
    int get_DayInterval() const;
    int get_HourInterval() const;
    int get_MinuteInterval() const;
    int get_SecondInterval() const;
};