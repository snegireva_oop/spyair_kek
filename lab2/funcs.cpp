#include "header.h"

calendar::calendar() {
    time_t sec = time(NULL);
    tm *time = localtime(&sec);
    years = time->tm_year + 1900;
    months = static_cast<month>(time->tm_mon);
    days = time->tm_yday;
    hours = time->tm_hour;
    minutes = time->tm_min;
    seconds = time->tm_sec;
}

calendar::calendar(int year, int mon, int day, int ho, int min, int sec) {
    norm_data(year, mon, day, ho, min, sec);
}

void calendar::norm_seconds(int &min, int &sec){
    if (sec >= 0){
        seconds = sec % 60;
        min += (sec / 60);
    }
    else{
        seconds = 60 * (sec % 60 != 0) + sec % 60;
        min = min + sec / 60 - (sec % 60 && 1);
    }
}
void calendar::norm_minutes(int &ho, int &min){
    if (min >= 0){
        minutes = min % 60;
        ho += (min / 60);
    }
    else{
        minutes = 60 * (min % 60 && 1) + min % 60;
        ho = ho + min / 60 - (min % 60 && 1);
    }
}
void calendar::norm_hours(int &day, int &ho){
    if (ho >= 0){
        hours = ho % 24;
        day += ho / 24;
    }
    else {
        hours = 24 * (ho & 24 && 1) + (ho % 24);
        day = day + ho / 24 - (ho % 24 && 1);
    }
}

void calendar::norm_data(int year, int mon, int day, int ho, int min, int sec) {
    norm_seconds(min, sec);
    norm_minutes(ho, min);
    norm_hours(day, ho);
    year = mon >= 0 ? (year + mon / 12) : year + mon / 12  - (mon % 12 && 1);     // переполнение(отриц.) даты
    mon = mon >= 0 ? mon % 12 : (mon % 12 ? 12 + mon % 12 : 0);
    int d = 28 + (mon + mon/8) % 2 + 2 % mon + 2 * (1 / mon);
    if (day >= 0) {
        while (d < day) {
            if ((year % 4 == 0 && year % 100 != 0 || year % 400 == 0) && mon == 2)
                day -= (d + 1);
            else
                day -= d;
            mon++;
            if (mon > 12) {
                year += (mon / 12);          //исключение
                mon = mon % 12;
            }
            d = 28 + (mon + mon / 8) % 2 + 2 % mon + 2 * (1 / mon);
        }
    } else {
        while (day < 0){
            if (mon == 0){
                mon = 12;
                year--;               //исключение
            }
            mon--;
            d = 28 + (mon + mon/8) % 2 + 2 % mon + 2 * (1 / mon);
            if ((year % 4 == 0 && year % 100 != 0 || year % 400 == 0) && mon == 2)
                day += (d + 1);
            else
                day += d;
        }
    }
    days = day;
    months = static_cast<month>(mon % 12);
    years = year + mon / 12;
    if(days == 0){
        d = 28 + (static_cast<int>(months) - 1 + (static_cast<int>(months)-1)/8) % 2 + 2 % (static_cast<int>(months)-1) + 2 * (1 / (static_cast<int>(months)-1));
        days = d;
        months = static_cast<month>(static_cast<int>(months)-1);
    }
    while (static_cast<int>(months) <= 0){
        months = static_cast<month>(static_cast<int>(months) + 12);
        years--;
    }
}

calendar calendar::add_seconds(int second) const{
    calendar cal(years, static_cast<int>(months), days, hours, minutes, seconds + second);
    return cal;
}
calendar calendar::add_minutes(int minute) const{
    calendar cal(years, static_cast<int>(months), days, hours, minutes + minute, seconds);
    return cal;
}
calendar calendar::add_hours(int hour) const{
    calendar cal(years, static_cast<int>(months), days, hours + hour, minutes, seconds);
    return cal;
}
calendar calendar::add_days(int day) const{
    calendar cal(years, static_cast<int>(months), days + day, hours, minutes, seconds);
    return cal;
}
calendar calendar::add_months(int month) const{
    calendar cal(years, static_cast<int>(months) + month, days, hours, minutes, seconds);
    return cal;
}
calendar calendar::add_years(int year) const{
    calendar cal(years + year, static_cast<int>(months), days, hours, minutes, seconds);
    return cal;
}

calendar::calendar(const calendar &obg){
    years = obg.years;
    months = obg.months;
    days = obg.days;
    hours = obg.hours;
    minutes = obg.minutes;
    seconds = obg.seconds;
}

int calendar::get_year() const{
    return years;
}
month calendar::get_month() const{
    return months;
}
int calendar::get_day() const{
    return days;
}
int calendar::get_hour() const{
    return hours;
}
int calendar::get_minutes() const{
    return minutes;
}
int calendar::get_second() const{
    return seconds;
}

calendar::calendar(u_int year, month month, u_int day){
    norm_data(year, static_cast<int>(month), day, 0, 0, 0);
}
calendar::calendar(u_int hour, u_int min, u_int sec){
    time_t s = time(NULL);
    tm *time = localtime(&s);
    years = time->tm_year + 1900;
    months = static_cast<month>(time->tm_mon);
    days = time->tm_yday;
    norm_data(years, static_cast<int>(months), days, hour, min, sec);
}

calendar& calendar::operator=(const calendar &data2) {
    years = data2.years;
    months = data2.months;
    days = data2.days;
    hours = data2.hours;
    minutes = data2.minutes;
    seconds = data2.seconds;
    return *this;
}

std::string calendar::toString() const{
    std::string out = "";
    out += (years/1000 + '0');
    out += (years%1000/100 + '0');
    out += (years%100/10 + '0');
    out += (years%10 + '0');
    out += "-";
    out += getstr_mon(months);
    out += "-";
    out += (days/10 + '0');
    out += (days%10 + '0');
    out += " ";
    out += (hours/10 + '0');
    out += (hours%10 + '0');
    out += "::";
    out += (minutes/10 + '0');
    out += (minutes%10 + '0');
    out += "::";
    out += (seconds/10 + '0');
    out += (seconds%10 + '0');
    return out;
}

calendar& calendar::operator++(){
    seconds++;
    norm_data(years, static_cast<int>(months), days, hours, minutes, seconds);
    return  *this;
}
calendar calendar::operator++(int){
    calendar temp = *this;
    ++(*this);
    return temp;
}
calendar& calendar::operator--(){
    seconds--;
    norm_data(years, static_cast<int>(months), days, hours, minutes, seconds);
    return  *this;
}
calendar  calendar::operator--(int){
    calendar temp = *this;
    --(*this);
    return temp;
}
std::ostream& operator<<(std::ostream& os, const calendar &data){
    std::string out = data.toString();
    os << out;
    return os;
}

DateInterval calendar::getInterval(const calendar &another) const{
    int year = (*this).years - another.years;
    int month = static_cast<int>((*this).months) - static_cast<int>(another.months);
    int day = (*this).days - another.days;
    int hour = (*this).hours - another.hours;
    int min = (*this).minutes - another.minutes;
    int sec = (*this).seconds - another.seconds;
    DateInterval interval(year, month, day, hour, min, sec);
    return interval;
}

calendar calendar::addInterval (const DateInterval &amp) const{
    int y = (*this).years + amp.get_YearInterval();
    int m = static_cast<int>((*this).months) + amp.get_MonthInterval();
    int d = (*this).days + amp.get_DayInterval();
    int h = (*this).hours + amp.get_HourInterval();
    int mi = (*this).minutes + amp.get_MinuteInterval();
    int s = (*this).seconds + amp.get_SecondInterval();
    calendar date(y,m,d,h,mi,s);
    return date;
}

calendar  calendar::operator+ (const DateInterval &amp) const{
    int y = (*this).years + amp.get_YearInterval();
    int m = static_cast<int>((*this).months) + amp.get_MonthInterval();
    int d = (*this).days + amp.get_DayInterval();
    int h = (*this).hours + amp.get_HourInterval();
    int mi = (*this).minutes + amp.get_MinuteInterval();
    int s = (*this).seconds + amp.get_SecondInterval();
    calendar date(y,m,d,h,mi,s);
    return date;
}
calendar& calendar::operator+=(const DateInterval &amp){
    years = years + amp.get_YearInterval();
    months = static_cast<month>(static_cast<int>(months) + amp.get_MonthInterval());
    days = days + amp.get_DayInterval();
    hours = hours + amp.get_HourInterval();
    minutes = minutes + amp.get_MinuteInterval();
    seconds = seconds + amp.get_SecondInterval();
    return *this;
}
calendar  calendar::operator- (const DateInterval &amp) const{
    int y = (*this).years - amp.get_YearInterval();
    int m = static_cast<int>((*this).months) - amp.get_MonthInterval();
    int d = (*this).days - amp.get_DayInterval();
    int h = (*this).hours - amp.get_HourInterval();
    int mi = (*this).minutes - amp.get_MinuteInterval();
    int s = (*this).seconds - amp.get_SecondInterval();
    calendar date(y,m,d,h,mi,s);
    return date;
}
calendar& calendar::operator-=(const DateInterval &amp){
    int y = (*this).years - amp.get_YearInterval();
    int m = static_cast<int>((*this).months) - amp.get_MonthInterval();
    int d = (*this).days - amp.get_DayInterval();
    int h = (*this).hours - amp.get_HourInterval();
    int mi = (*this).minutes - amp.get_MinuteInterval();
    int s = (*this).seconds - amp.get_SecondInterval();
    calendar date(y,m,d,h,mi,s);
    return date;
}

std::string calendar::formatDate (std::string format){
    size_t l = format.size();
    int i = 0;
    char check[6] = {0};
    while (i < l){
        int t = i;
        switch (format[i]) {
            case 'Y':
                if (check[0] == 1)
                    break;
                if (format[i + 1] != 'Y' && format[i + 2] != 'Y' && format[i + 3] != 'Y')
                    break;
                format[i] = years / 1000 + '0';
                format[i + 1] = (years % 1000) / 100 + '0';
                format[i + 2] = (years % 100) / 10 + '0';
                format[i + 3] = years % 10 + '0';
                i += 4;
                check[0] = 1;
                break;
            case 'M':
                if (check[1] == 1)
                    break;
                if (format[i + 1] != 'M')
                    break;
                if (format[i + 2] != 'M'){
                    format[i] = static_cast<int>(months)/10 + '0';
                    format[i + 1] = static_cast<int>(months)%10 + '0';
                    i += 2;
                }
                else{
                    std::string s = getstr_mon(months);
                    format[i] = s[0];
                    format[i + 1] = s[1];
                    format[i + 2] = s[2];
                    i += 3;
                }
                check[1] = 1;
                break;
            case 'D':
                if (check[2] == 1)
                    break;
                if (format[i + 1] != 'D')
                    break;
                else{
                    format[i] = days/10 + '0';
                    format[i + 1] = days%10 + '0';
                    i += 2;
                }
                check[2] = 1;
                break;
            case 'h':
                if (check[3] == 1)
                    break;
                if (format[i + 1] != 'h')
                    break;
                else{
                    format[i] = hours/10 + '0';
                    format[i + 1] = hours%10 + '0';
                    i += 2;
                }
                check[3] = 1;
                break;
            case 'm':
                if (check[4] == 1)
                    break;
                if (format[i + 1] != 'm')
                    break;
                else{
                    format[i] = minutes/10 + '0';
                    format[i + 1] = minutes%10 + '0';
                    i += 2;
                }
                check[4] = 1;
                break;
            case 's':
                if (check[5] == 1)
                    break;
                if (format[i + 1] != 's')
                    break;
                else{
                    format[i] = seconds/10 + '0';
                    format[i + 1] = seconds%10 + '0';
                    i += 2;
                }
                check[5] = 1;
                break;
        }
        if (i == t)
            i++;
    }
    return format;
}

std::string calendar::getstr_mon(enum month mon) const{
    switch (mon){
        case month::JAN: return "JAN";
        case month::FEB: return "FEB";
        case month::MAR: return "MAR";
        case month::APR: return "APR";
        case month::MAY: return "MAY";
        case month::JUN: return "JUN";
        case month::JUL: return "JUL";
        case month::AGU: return "AGU";
        case month::SEP: return "SEP";
        case month::OCT: return "OCT";
        case month::NOV: return "NOV";
        case month::DES: return "DES";
    }
}